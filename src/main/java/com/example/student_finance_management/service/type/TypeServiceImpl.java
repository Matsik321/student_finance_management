package com.example.student_finance_management.service.type;

import com.example.student_finance_management.entities.Type;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class TypeServiceImpl implements TypeService {

    @Override
    public List<Type> get() {
        return Arrays.asList(Type.values());
    }

    @Override
    public Type get(Long id) {
        return null;
    }
}
