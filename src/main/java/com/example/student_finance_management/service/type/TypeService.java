package com.example.student_finance_management.service.type;

import com.example.student_finance_management.entities.Type;
import com.example.student_finance_management.service.ReadService;

public interface TypeService extends ReadService<Type> {
}
