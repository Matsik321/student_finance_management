package com.example.student_finance_management.service;

import java.util.List;

public interface ReadService<T> {

    List<T> get();

    T get(Long id);
}
