package com.example.student_finance_management.service.wallet;

import com.example.student_finance_management.entities.Wallet;
import com.example.student_finance_management.entities.security.User;
import com.example.student_finance_management.repository.WalletRepository;
import com.example.student_finance_management.service.models.AddDeleteResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WalletServiceImpl implements WalletService {

    @Autowired
    WalletRepository walletRepository;

    @Override
    public List<Wallet> get(Long userId) {
        return walletRepository.findByUser_Id(userId);
    }

    @Override
    public AddDeleteResponse add(User user, Wallet wallet) {
        wallet.setUser(user);
        try {
            walletRepository.save(wallet);
        }catch(Exception e){
            return new AddDeleteResponse("Wallet already exist", false);
        }
        return new AddDeleteResponse("", true);
    }
}
