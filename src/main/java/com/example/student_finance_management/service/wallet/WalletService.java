package com.example.student_finance_management.service.wallet;

import com.example.student_finance_management.entities.Wallet;
import com.example.student_finance_management.entities.security.User;
import com.example.student_finance_management.service.models.AddDeleteResponse;

import java.util.List;

public interface WalletService {

    List<Wallet> get(Long userId);

    AddDeleteResponse add(User user, Wallet wallet);
}
