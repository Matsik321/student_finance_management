package com.example.student_finance_management.service.transaction;

import com.example.student_finance_management.entities.Dates;
import com.example.student_finance_management.entities.Transaction;
import com.example.student_finance_management.entities.TransactionType;
import com.example.student_finance_management.entities.Wallet;
import com.example.student_finance_management.entities.security.User;
import com.example.student_finance_management.repository.DateRepository;
import com.example.student_finance_management.repository.TransactionRepository;
import com.example.student_finance_management.repository.WalletRepository;
import com.example.student_finance_management.service.models.AddDeleteResponse;
import com.example.student_finance_management.service.models.AddTransactionModel;
import com.example.student_finance_management.service.models.TransactionResponse;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TransactionServiceImpl implements TransactionService {

    private static final int NUMBER_OF_VISIBLE_PAGES = 3;

    @Autowired
    TransactionRepository transactionRepository;

    @Autowired
    WalletRepository walletRepository;

    @Autowired
    DateRepository dateRepository;

    @Override
    public List<Transaction> get() {
        return Lists.newArrayList(transactionRepository.findAll());
    }

    @Override
    public Transaction get(Long id) {
        return transactionRepository.findByTransactionId(id);
    }

    @Override
    public void delete(Long id) {
        transactionRepository.delete(id);
    }

    @Override
    public TransactionResponse getPageOfTransaction(Long id, int page, int size) {
        Page<Transaction> result = transactionRepository.findAll(new PageRequest(page, size,
                new Sort(
                        new Sort.Order(Sort.Direction.DESC, "date.year"),
                        new Sort.Order(Sort.Direction.DESC, "date.month"),
                        new Sort.Order(Sort.Direction.DESC, "date.day"))));
        List<Integer> visiblePages = getVisiblePages(page, result.getTotalPages());
        return new TransactionResponse(result.getContent(), result.getTotalPages(), visiblePages);
    }

    @Override
    public AddDeleteResponse addTransaction(User user, AddTransactionModel addTransactionModel) {
        Wallet wallet = findWallet(addTransactionModel.getWallet());
        Dates date = parseDate(addTransactionModel.getDate());
        return handleSaveTransaction(user, addTransactionModel, wallet, date);
    }

    @Override
    public AddDeleteResponse deleteTransaction(Long transactionId) {
        try {
            transactionRepository.delete(transactionId);
        } catch (EmptyResultDataAccessException e) {
            return new AddDeleteResponse("Transaction don't exit", false);
        }
        return new AddDeleteResponse("", true);
    }

    @Override
    public TransactionResponse getPageOfTransaction(Long id, int page, int size, TransactionType type, Long wallet) {
        Page<Transaction> result = transactionRepository.findByWallet_WalletIdAndType(wallet, type, new PageRequest(page, size,
                new Sort(
                        new Sort.Order(Sort.Direction.DESC, "date.year"),
                        new Sort.Order(Sort.Direction.DESC, "date.month"),
                        new Sort.Order(Sort.Direction.DESC, "date.day"))));
        List<Integer> visiblePages = getVisiblePages(page, result.getTotalPages());
        return new TransactionResponse(result.getContent(), result.getTotalPages(), visiblePages);
    }

    private AddDeleteResponse handleSaveTransaction(User user, AddTransactionModel addTransactionModel, Wallet wallet, Dates date) {
        try {
            transactionRepository.save(new Transaction(addTransactionModel, wallet, user, date));
        } catch (Exception e) {
            return new AddDeleteResponse("Transaction already exist", false);
        }
        updateWallet(addTransactionModel, wallet);
        return new AddDeleteResponse("", true);
    }

    private void updateWallet(AddTransactionModel addTransactionModel, Wallet wallet) {
        wallet.setBallance(wallet.getBallance() + addTransactionModel.getCost());
        walletRepository.save(wallet);
    }

    private Dates parseDate(String date) {
        String[] splitetDate = date.split("-");
        int year = Integer.valueOf(splitetDate[0]);
        int month = Integer.valueOf(splitetDate[1]);
        int day = Integer.valueOf(splitetDate[2]);
        Optional<Dates> dateFromDataBase = dateRepository.findByYearAndMonthAndDay(year, month, day);
        return dateFromDataBase.orElseGet(() -> new Dates(year, month, day));
    }

    private Wallet findWallet(Long wallet) {
        return walletRepository.findOne(wallet);
    }

    private List<Integer> getVisiblePages(int page, int totalPages) {
        List<Integer> visiblePages = new ArrayList<>();
        for (int i = page + NUMBER_OF_VISIBLE_PAGES; i >= page - NUMBER_OF_VISIBLE_PAGES; i--) {
            if (i >= 0 && i < totalPages) {
                visiblePages.add(i + 1);
            }
        }
        visiblePages.sort(Integer::compare);
        return visiblePages;
    }
}
