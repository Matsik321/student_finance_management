package com.example.student_finance_management.service.transaction;

import com.example.student_finance_management.entities.Transaction;
import com.example.student_finance_management.entities.TransactionType;
import com.example.student_finance_management.entities.security.User;
import com.example.student_finance_management.service.RestService;
import com.example.student_finance_management.service.models.AddDeleteResponse;
import com.example.student_finance_management.service.models.AddTransactionModel;
import com.example.student_finance_management.service.models.TransactionResponse;

public interface TransactionService extends RestService<Transaction> {

    TransactionResponse getPageOfTransaction(Long id, int page, int size);

    AddDeleteResponse addTransaction(User user, AddTransactionModel addTransactionModel);

    AddDeleteResponse deleteTransaction(Long transactionId);

    TransactionResponse getPageOfTransaction(Long id, int page, int size, TransactionType type, Long wallet);
}
