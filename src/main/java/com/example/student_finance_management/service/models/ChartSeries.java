package com.example.student_finance_management.service.models;

import java.util.List;

public class ChartSeries {

    private String label;

    private List<Double> data;

    public ChartSeries(String label, List<Double> data) {
        this.label = label;
        this.data = data;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<Double> getData() {
        return data;
    }

    public void setData(List<Double> data) {
        this.data = data;
    }
}
