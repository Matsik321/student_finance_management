package com.example.student_finance_management.service.models;

import com.example.student_finance_management.entities.Element;
import com.example.student_finance_management.entities.Product;
import com.example.student_finance_management.entities.Type;

public class ElementsResponse {

    public ElementsResponse(Element elements) {
        this.elementId = elements.getElementId();
        this.quantity = elements.getQuantity();
        this.cost = elements.getCost();
        this.product = elements.getProduct();
        this.type = elements.getType();
    }

    private Long elementId;

    private double quantity;

    private double cost;

    private String image;

    private Type type;

    private Product product;
    public Long getElementId() {
        return elementId;
    }

    public void setElementId(Long elementId) {
        this.elementId = elementId;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
