package com.example.student_finance_management.service.models;

public class AddDeleteResponse {
    private String errorDescription;

    private boolean result;

    public AddDeleteResponse(String errorDescription, boolean result) {
        this.errorDescription = errorDescription;
        this.result = result;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }
}
