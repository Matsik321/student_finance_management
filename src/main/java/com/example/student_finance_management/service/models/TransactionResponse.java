package com.example.student_finance_management.service.models;

import com.example.student_finance_management.entities.Transaction;

import java.util.List;

public class TransactionResponse {

    public TransactionResponse(List<Transaction> transactionList, int numberOfPages, List<Integer> visiblePages) {
        this.transactionList = transactionList;
        this.numberOfPages = numberOfPages;
        this.visiblePages = visiblePages;
    }

    private List<Transaction> transactionList;

    private int numberOfPages;

    private List<Integer> visiblePages;

    public List<Integer> getVisiblePages() {
        return visiblePages;
    }

    public void setVisiblePages(List<Integer> visiblePages) {
        this.visiblePages = visiblePages;
    }

    public List<Transaction> getTransactionList() {
        return transactionList;
    }

    public void setTransactionList(List<Transaction> transactionList) {
        this.transactionList = transactionList;
    }

    public int getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(int numberOfPages) {
        this.numberOfPages = numberOfPages;
    }
}
