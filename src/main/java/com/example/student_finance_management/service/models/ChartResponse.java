package com.example.student_finance_management.service.models;

import java.util.List;

public class ChartResponse {

    private List<String> xAxisData;

    private List<ChartSeries> chartSeriesList;

    public ChartResponse(List<String> xAxisData, List<ChartSeries> chartSeriesList) {
        this.xAxisData = xAxisData;
        this.chartSeriesList = chartSeriesList;
    }

    public List<String> getxAxisData() {
        return xAxisData;
    }

    public void setxAxisData(List<String> xAxisData) {
        this.xAxisData = xAxisData;
    }

    public List<ChartSeries> getChartSeriesList() {
        return chartSeriesList;
    }

    public void setChartSeriesList(List<ChartSeries> chartSeriesList) {
        this.chartSeriesList = chartSeriesList;
    }
}
