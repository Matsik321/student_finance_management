package com.example.student_finance_management.service.statistic;

import com.example.student_finance_management.entities.Element;
import com.example.student_finance_management.entities.Transaction;
import com.example.student_finance_management.entities.Type;
import com.example.student_finance_management.repository.DateRepository;
import com.example.student_finance_management.repository.TransactionElementsRepository;
import com.example.student_finance_management.repository.TransactionRepository;
import com.example.student_finance_management.service.models.ChartResponse;
import com.example.student_finance_management.service.models.ChartSeries;
import com.example.student_finance_management.service.models.StatisticRequestData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class StatisticServiceImpl implements StatisticService {

    @Autowired
    DateRepository dateRepository;
    @Autowired
    TransactionRepository transactionRepository;
    @Autowired
    TransactionElementsRepository transactionElementsRepository;

    @Override
    public double getSpend(Type type) {
        return transactionElementsRepository
                .findByType(type)
                .stream()
                .mapToDouble(Element::getCost)
                .sum();
    }

    @Override
    public double getSpend(Type type, int year) {
        return dateRepository
                .findByYear(year)
                .stream()
                .map(date -> transactionRepository.findByDate_Id(date.getId()))
                .flatMap(Collection::stream)
                .map(Transaction::getElements)
                .flatMap(Collection::stream)
                .filter(element -> element.getType() == type)
                .mapToDouble(Element::getCost)
                .sum();
    }

    @Override
    public double getSpend(Type type, int year, int month) {
        return dateRepository
                .findByYearAndMonth(year, month)
                .stream()
                .map(date -> transactionRepository.findByDate_Id(date.getId()))
                .flatMap(Collection::stream)
                .map(Transaction::getElements)
                .flatMap(Collection::stream)
                .filter(element -> element.getType() == type)
                .mapToDouble(Element::getCost)
                .sum();
    }

    @Override
    public double getSpend(int year, int month) {
        return dateRepository.findByYearAndMonth(year, month).stream()
                .map(dates -> transactionRepository.findByDate_Id(dates.getId()))
                .flatMap(Collection::stream)
                .map(Transaction::getElements)
                .flatMap(Collection::stream)
                .mapToDouble(Element::getCost)
                .sum();
    }

    @Override
    public ChartResponse getSpend(StatisticRequestData statisticRequestData) {
        List<ShortDates> datesToProcess = takeDates(statisticRequestData);
        List<ChartSeries> chartSeries = getChartSeries(statisticRequestData, datesToProcess);
        List<String> xAxisData = getAxisData(datesToProcess);
        return new ChartResponse(xAxisData, chartSeries);

    }

    private List<String> getAxisData(List<ShortDates> datesToProcess) {
        return datesToProcess
                .stream()
                .map(shortDate -> shortDate.getMonth() + " " + shortDate.getYear())
                .collect(Collectors.toList());
    }

    private List<ChartSeries> getChartSeries(StatisticRequestData statisticRequestData, List<ShortDates> datesToProcess) {
        return statisticRequestData
                    .getTypes()
                    .stream()
                    .map(type -> new ChartSeries(type.name(), createChartSerie(datesToProcess, type)))
                    .collect(Collectors.toList());
    }

    private List<Double> createChartSerie(List<ShortDates> datesToProcess, Type type) {
        return datesToProcess
                .stream()
                .map(dates ->  getSpend(type, dates.getYear(), dates.getMonth()))
                .collect(Collectors.toList());
    }

    private List<ShortDates> takeDates(StatisticRequestData statisticRequestData) {
        List<ShortDates> result = new ArrayList<>();
        int year = statisticRequestData.getBeginYear();
        int month = statisticRequestData.getBeginMonth();
        boolean end = false;
        while (!end) {
            result.add(new ShortDates(month, year));
            if (month == statisticRequestData.getEndMonth() && year == statisticRequestData.getEndYear()) {
                end = true;
            } else {
                if (month == 12) {
                    month = 1;
                    year++;
                } else {
                    month++;
                }
            }
        }
        return result;
    }
}
