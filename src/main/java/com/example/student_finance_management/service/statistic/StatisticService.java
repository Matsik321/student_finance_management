package com.example.student_finance_management.service.statistic;

import com.example.student_finance_management.entities.Type;
import com.example.student_finance_management.service.models.ChartResponse;
import com.example.student_finance_management.service.models.StatisticRequestData;

public interface StatisticService {

    double getSpend(Type type);

    double getSpend(Type type, int year);

    double getSpend(Type type, int year, int month);

    double getSpend(int year, int month);

    ChartResponse getSpend(StatisticRequestData statisticRequestData);
}
