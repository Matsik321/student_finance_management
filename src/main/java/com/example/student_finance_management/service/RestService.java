package com.example.student_finance_management.service;

public interface RestService<T> extends ReadService<T> {

    void delete(Long id);
}
