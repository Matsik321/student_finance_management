package com.example.student_finance_management.service.user;

import com.example.student_finance_management.entities.security.User;

public interface UserService {

    User add(User user);
}
