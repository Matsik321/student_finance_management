package com.example.student_finance_management.service.user;

import com.example.student_finance_management.entities.security.Authority;
import com.example.student_finance_management.entities.security.AuthorityName;
import com.example.student_finance_management.entities.security.User;
import com.example.student_finance_management.repository.AuthorityRepository;
import com.example.student_finance_management.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    public static final boolean ENABLED = true;
    public static final String EMPTY_LASTNAME = "";
    public static final String EMPTY_FIRSTNAME = "";

    @Autowired
    UserRepository userRepository;

    @Autowired
    private AuthorityRepository authorityRepository;

    public User add(User user){
        return userRepository.save(proccesssUser(user));
    }

    private User proccesssUser(User user) {
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String hashedPassword = passwordEncoder.encode(user.getPassword());
        User newUser = new User(user.getUsername(), hashedPassword, EMPTY_FIRSTNAME, EMPTY_LASTNAME, ENABLED,  Calendar.getInstance().getTime());
        addUserRole(newUser);
        return newUser;
    }

    private void addUserRole(User createdUser) {
        List<Authority> authorities = new ArrayList<>();
        authorities.add(authorityRepository.findByName(AuthorityName.ROLE_USER));
        createdUser.setAuthorities(authorities);
    }
}
