package com.example.student_finance_management.service.transaction_elements;

import com.example.student_finance_management.entities.Element;
import com.example.student_finance_management.entities.ElementBuilder;
import com.example.student_finance_management.entities.Product;
import com.example.student_finance_management.entities.Transaction;
import com.example.student_finance_management.repository.ProductRepository;
import com.example.student_finance_management.repository.TransactionElementsRepository;
import com.example.student_finance_management.repository.TransactionRepository;
import com.example.student_finance_management.service.models.AddDeleteResponse;
import com.example.student_finance_management.service.models.AddTransactionElement;
import com.example.student_finance_management.service.models.ElementsResponse;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TransactionElementServiceImpl implements TransactionElementsService {

    @Autowired
    private TransactionElementsRepository transactionElementsRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    @Override
    public List<ElementsResponse> get() {
        return mapToElementResponse(Lists.newArrayList(transactionElementsRepository.findAll()));
    }

    @Override
    public ElementsResponse get(Long id) {
        return new ElementsResponse(transactionElementsRepository.findByElementId(id));
    }

    @Override
    public void delete(Long id) {
        transactionElementsRepository.delete(id);
    }

    @Override
    public AddDeleteResponse add(AddTransactionElement transactionElementModel){
        Product product = productRepository.findByProductId(transactionElementModel.getProductId());
        Transaction transaction = transactionRepository.findByTransactionId(transactionElementModel.getTransactionId());
        Element element = new ElementBuilder()
                .setProduct(product)
                .setTransaction(transaction)
                .setCost(transactionElementModel.getCost())
                .setQuantity(transactionElementModel.getQuantity())
                .setType(transactionElementModel.getType())
                .createElement();
        transactionElementsRepository.save(element);
        updateCost(transactionElementModel, transaction);
        return new AddDeleteResponse("", true);
    }

    private void updateCost(AddTransactionElement transactionElementModel, Transaction transaction) {
        transaction.setCost(transaction.getCost() + transactionElementModel.getCost() * transactionElementModel.getQuantity());
        transactionRepository.save(transaction);
    }

    @Override
    public List<ElementsResponse> getByTransactionId(Long id) {
        return mapToElementResponse(transactionElementsRepository.findByTransaction_TransactionId(id));
    }

    private List<ElementsResponse> mapToElementResponse(List<Element> elements){
        return elements
                .stream()
                .map(ElementsResponse::new)
                .collect(Collectors.toList());
    }
}
