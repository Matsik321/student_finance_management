package com.example.student_finance_management.service.transaction_elements;

import com.example.student_finance_management.service.RestService;
import com.example.student_finance_management.service.models.AddDeleteResponse;
import com.example.student_finance_management.service.models.AddTransactionElement;
import com.example.student_finance_management.service.models.ElementsResponse;

import java.util.List;

public interface TransactionElementsService extends RestService<ElementsResponse> {

    List<ElementsResponse> getByTransactionId(Long id);

    AddDeleteResponse add(AddTransactionElement transaction);
}
