package com.example.student_finance_management.service.category;

import com.example.student_finance_management.entities.Category;
import com.example.student_finance_management.service.ReadService;

import java.util.List;

public interface CategoryService extends ReadService<Category> {

    List<Category> getSubcategories(Long parentId);
}
