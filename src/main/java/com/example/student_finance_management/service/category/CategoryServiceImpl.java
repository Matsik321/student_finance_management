package com.example.student_finance_management.service.category;

import com.example.student_finance_management.entities.Category;
import com.example.student_finance_management.repository.CategoryRepository;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    CategoryRepository categoryRepository;

    @Override
    public List<Category> get() {
        return Lists.newArrayList(categoryRepository.findAll());
    }

    @Override
    public Category get(Long id) {
        return categoryRepository.findByCategoryId(id);
    }

    @Override
    public List<Category> getSubcategories(Long parentId) {
        return categoryRepository.findByParent(parentId);
    }
}
