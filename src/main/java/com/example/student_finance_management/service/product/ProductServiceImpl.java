package com.example.student_finance_management.service.product;

import com.example.student_finance_management.entities.Product;
import com.example.student_finance_management.repository.ProductRepository;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductRepository productRepository;

    @Override
    public List<Product> get() {
        return Lists.newArrayList(productRepository.findAll());
    }

    @Override
    public Product get(Long id) {
        return productRepository.findByProductId(id);
    }

    @Override
    public void delete(Long id) {
        productRepository.delete(id);
    }

    @Override
    public List<Product> findCategoryProducts(Long categoryId) {
        return productRepository.findByCategory_CategoryId(categoryId);
    }
}
