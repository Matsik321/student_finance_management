package com.example.student_finance_management.service.product;

import com.example.student_finance_management.entities.Product;
import com.example.student_finance_management.service.RestService;

import java.util.List;

public interface ProductService extends RestService<Product> {

    List<Product> findCategoryProducts(Long categoryId);
}
