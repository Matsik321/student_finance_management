package com.example.student_finance_management.api;

import com.example.student_finance_management.entities.Product;
import com.example.student_finance_management.service.product.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/product")
@CrossOrigin(origins = "*")
public class ProductApi {

    @Autowired
    private ProductService productService;

    @RequestMapping(value = "/category/{categoryId}", method = RequestMethod.GET)
    public Collection<Product> getProductsByCategoryId(@PathVariable Long categoryId){
        return productService
                .findCategoryProducts(categoryId);
    }

    @RequestMapping(value = "/{productId}", method = RequestMethod.GET)
    public Product getProductsById(@PathVariable Long productId){
        return productService
                .get(productId);
    }
}
