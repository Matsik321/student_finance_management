package com.example.student_finance_management.api;

import com.example.student_finance_management.authentication.JwtTokenUtil;
import com.example.student_finance_management.entities.TransactionType;
import com.example.student_finance_management.entities.security.User;
import com.example.student_finance_management.repository.UserRepository;
import com.example.student_finance_management.service.models.AddDeleteResponse;
import com.example.student_finance_management.service.models.AddTransactionModel;
import com.example.student_finance_management.service.models.TransactionResponse;
import com.example.student_finance_management.service.transaction.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/transactions")
@CrossOrigin(origins = "*")
public class TransactionsApi {

    @Value("${jwt.header}")
    private String tokenHeader;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TransactionService transactionService;

    @RequestMapping(value = "{page}/{size}", method = RequestMethod.GET)
    public ResponseEntity<?> getUserTransactions(HttpServletRequest request, @PathVariable("page") int page,  @PathVariable("size") int size) {
        User user = getUser(request);
        TransactionResponse transactionResponse = transactionService.getPageOfTransaction(user.getId(), page, size);
        return ResponseEntity.ok(transactionResponse);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> addTransaction(HttpServletRequest request, @RequestBody AddTransactionModel addTransactionModel) {
        User user = getUser(request);
        AddDeleteResponse addTransactionResponse = transactionService.addTransaction(user, addTransactionModel);
        return ResponseEntity.ok(addTransactionResponse);
    }

    @RequestMapping(value = "{page}/{size}/{wallet}/{type}", method = RequestMethod.GET)
    public ResponseEntity<?> getUserTransactionsFiltered(HttpServletRequest request, @PathVariable("page") int page,
                                                         @PathVariable("size") int size, @PathVariable("wallet") Long walletId,
                                                         @PathVariable("type") TransactionType type) {
        User user = getUser(request);
        TransactionResponse transactionResponse = transactionService.getPageOfTransaction(user.getId(), page, size, type, walletId);
        return ResponseEntity.ok(transactionResponse);
    }

    private User getUser(HttpServletRequest request) {
        String token = request.getHeader(tokenHeader);
        String username = jwtTokenUtil.getUsernameFromToken(token);
        return userRepository.findByUsername(username);
    }
}
