package com.example.student_finance_management.api;

import com.example.student_finance_management.authentication.JwtTokenUtil;
import com.example.student_finance_management.entities.Wallet;
import com.example.student_finance_management.entities.security.User;
import com.example.student_finance_management.repository.UserRepository;
import com.example.student_finance_management.service.wallet.WalletService;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/wallets")
@CrossOrigin(origins = "*")
public class WalletApi {
    @Value("${jwt.header}")
    private String tokenHeader;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private WalletService walletService;


    @RequestMapping(method = RequestMethod.GET)
    public List<Wallet> getWallets(HttpServletRequest request) {
        String token = request.getHeader(tokenHeader);
        String username = jwtTokenUtil.getUsernameFromToken(token);
        Long userId = userRepository.findByUsername(username).getId();
        return Lists.newArrayList(walletService.get(userId));
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> addWallets(HttpServletRequest request, @RequestBody Wallet wallet) {
        String token = request.getHeader(tokenHeader);
        String username = jwtTokenUtil.getUsernameFromToken(token);
        User user = userRepository.findByUsername(username);
        return ResponseEntity.ok(walletService.add(user, wallet));
    }


}
