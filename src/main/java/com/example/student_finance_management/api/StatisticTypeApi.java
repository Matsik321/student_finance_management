package com.example.student_finance_management.api;

import com.example.student_finance_management.entities.Type;
import com.example.student_finance_management.service.models.ChartResponse;
import com.example.student_finance_management.service.models.StatisticRequestData;
import com.example.student_finance_management.service.statistic.StatisticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/spend")
@CrossOrigin(origins = "*")
public class StatisticTypeApi {

    @Autowired
    private StatisticService statisticService;

    @RequestMapping(value = "/{type}", method = RequestMethod.GET)
    public double getSpend(@PathVariable Type type){
        return statisticService.getSpend(type);
    }

    @RequestMapping(value = "/{type}/{year}", method = RequestMethod.GET)
    public double getSpend(@PathVariable Type type, @PathVariable int year) {
        return statisticService.getSpend(type, year);
    }

    @RequestMapping(value = "/{type}/{year}/{month}", method = RequestMethod.GET)
    public double getSpend(@PathVariable Type type, @PathVariable int year, @PathVariable int month) {
        return statisticService.getSpend(type, year, month);
    }

    @RequestMapping(value = "/{year}/{month}", method = RequestMethod.GET)
    public double getSpend(@PathVariable int year, @PathVariable int month) {
        return statisticService.getSpend(year, month);
    }

    @RequestMapping(value = "/chart", method = RequestMethod.POST)
    public ChartResponse getSpend(@RequestBody StatisticRequestData statisticRequestData) {
        return statisticService.getSpend(statisticRequestData);
    }
}
