package com.example.student_finance_management.api;

import com.example.student_finance_management.entities.Category;
import com.example.student_finance_management.service.category.CategoryService;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/category")
@CrossOrigin(origins = "*")
public class CategoryApi {

    @Autowired
    private CategoryService categoryService;

    @RequestMapping(value = "/{categoryId}", method = RequestMethod.GET)
    public Collection<Category> getSubCategoriesById(@PathVariable Long categoryId){
        return Lists.newArrayList(categoryService
                .getSubcategories(categoryId));
    }
}
