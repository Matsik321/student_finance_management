package com.example.student_finance_management.api;

import com.example.student_finance_management.service.models.AddTransactionElement;
import com.example.student_finance_management.service.transaction_elements.TransactionElementsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")
public class TransactionElementsApi {

    @Autowired
    private TransactionElementsService transactionElementsService;

    @RequestMapping(value = "TransactionElements/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getUserTransactions(@PathVariable("id") Long id){
        return ResponseEntity.ok(transactionElementsService.getByTransactionId(id));
    }

    @RequestMapping(value = "TransactionElements/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteUserTransactions(@PathVariable("id") Long id){
        transactionElementsService.delete(id);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "TransactionElements", method = RequestMethod.POST)
    public ResponseEntity<?> addUserTransactions(@RequestBody AddTransactionElement transaction){
        transactionElementsService.add(transaction);
        return ResponseEntity.ok().build();
    }
}