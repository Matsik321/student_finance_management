package com.example.student_finance_management;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

import java.util.HashMap;

@SpringBootApplication
public class StudentFinanceManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudentFinanceManagementApplication.class, args);
	}
}
