package com.example.student_finance_management.repository;

import com.example.student_finance_management.entities.Category;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface CategoryRepository extends CrudRepository<Category, Long> {

    List<Category> findByParent(Long parentId);

    Category findByCategoryId(Long id);
}