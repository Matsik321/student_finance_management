package com.example.student_finance_management.repository;

import com.example.student_finance_management.entities.security.Authority;
import com.example.student_finance_management.entities.security.AuthorityName;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorityRepository  extends JpaRepository<Authority, Long> {
    Authority findByName(AuthorityName name);
}
