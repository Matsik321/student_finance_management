package com.example.student_finance_management.repository;

import com.example.student_finance_management.entities.Wallet;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface WalletRepository extends CrudRepository<Wallet, Long> {

    List<Wallet> findByUser_Id(Long userId);
}
