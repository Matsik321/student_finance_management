package com.example.student_finance_management.repository;

import com.example.student_finance_management.entities.Dates;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface DateRepository extends CrudRepository<Dates, Long> {

    List<Dates> findByYearAndMonth(int year, int month);

    List<Dates> findByYear(int year);

    Optional<Dates> findByYearAndMonthAndDay(int year, int month, int day);
}
