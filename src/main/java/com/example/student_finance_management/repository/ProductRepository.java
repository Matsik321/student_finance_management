package com.example.student_finance_management.repository;

import com.example.student_finance_management.entities.Product;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProductRepository extends CrudRepository<Product, Long> {

    List<Product> findByCategory_CategoryId(Long categoryId);

    Product findByProductId(Long productId);
}
