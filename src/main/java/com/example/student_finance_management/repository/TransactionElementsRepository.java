package com.example.student_finance_management.repository;

import com.example.student_finance_management.entities.Element;
import com.example.student_finance_management.entities.Type;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TransactionElementsRepository extends CrudRepository<Element, Long> {

    List<Element> findByTransaction_TransactionId(long transactionId);

    Element findByElementId(Long id);

    List<Element> findByType(Type type);
}
