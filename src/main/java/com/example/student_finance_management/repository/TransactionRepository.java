package com.example.student_finance_management.repository;

import com.example.student_finance_management.entities.Transaction;
import com.example.student_finance_management.entities.TransactionType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TransactionRepository extends CrudRepository<Transaction, Long> {

    List<Transaction> findByUser_Id(Long userId);

    Transaction findByTransactionId(Long transactionId);

    Page<Transaction> findAll(Pageable pageable);

    Page<Transaction> findByWallet_WalletIdAndType(Long walletName, TransactionType type, Pageable pageable);

    List<Transaction> findByDate_Id(Long dateId);
}
