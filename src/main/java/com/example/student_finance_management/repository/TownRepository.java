package com.example.student_finance_management.repository;

import com.example.student_finance_management.entities.Town;
import org.springframework.data.repository.CrudRepository;

public interface TownRepository extends CrudRepository<Town, Long> {
}
