package com.example.student_finance_management.entities;

public enum TransactionType {
    EXPENSE, INCOME
}
