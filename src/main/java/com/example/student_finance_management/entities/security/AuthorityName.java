package com.example.student_finance_management.entities.security;

public enum AuthorityName {
    ROLE_USER, ROLE_ADMIN
}