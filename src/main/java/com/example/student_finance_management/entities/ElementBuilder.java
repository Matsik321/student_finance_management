package com.example.student_finance_management.entities;

public class ElementBuilder {

    private double quantity;
    private double cost;
    private Type type;
    private Product product;
    private Transaction transaction;

    public ElementBuilder setQuantity(double quantity) {
        this.quantity = quantity;
        return this;
    }

    public ElementBuilder setCost(double cost) {
        this.cost = cost;
        return this;
    }

    public ElementBuilder setType(Type type) {
        this.type = type;
        return this;
    }

    public ElementBuilder setProduct(Product product) {
        this.product = product;
        return this;
    }

    public ElementBuilder setTransaction(Transaction transaction) {
        this.transaction = transaction;
        return this;
    }

    public Element createElement() {
        return new Element(quantity, cost, type, product, transaction);
    }
}