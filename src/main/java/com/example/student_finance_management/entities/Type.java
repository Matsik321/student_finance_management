package com.example.student_finance_management.entities;

public enum Type {
    PARTY, FOOD, FLAT, CLEANING
}
