package com.example.student_finance_management.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "dates", uniqueConstraints={
        @UniqueConstraint(columnNames = {"year", "month", "day"})
})
public class Dates implements Serializable{

    @Id
    @Column(name = "date_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private int year;
    private int month;
    private int day;

    private Dates() {
    }

    public Dates(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int String) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int String) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int String) {
        this.day = day;
    }
}
