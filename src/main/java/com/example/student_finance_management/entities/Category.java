package com.example.student_finance_management.entities;

import javax.annotation.Nullable;
import javax.persistence.*;
import java.util.List;
@Entity
@Table(name = "category")
public class Category {

    @Id
    @Column(name = "category_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long categoryId;

    private String name;

    private String image;

    @OneToMany(mappedBy = "category")
    private List<Product> products;

    @OneToMany
    @JoinColumn(name = "parent")
    private List<Category> categories;

    private Long parent;

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getParent() {
        return parent;
    }

    public void setParent(Long parent) {
        this.parent = parent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Category() {
    }

    public Category(String name, String image) {

        this.name = name;
        this.image = image;
    }
}
