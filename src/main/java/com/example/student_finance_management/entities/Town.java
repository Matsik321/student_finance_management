package com.example.student_finance_management.entities;

import javax.persistence.*;

@Entity
@Table(name = "Town")
public class Town {
    @Id
    @Column(name = "town_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long townId;

    private String name;

    public Long getTownId() {
        return townId;
    }

    public void setTownId(Long id) {
        this.townId = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
