package com.example.student_finance_management.entities;

public enum Unit {
    Kilogram, Liter, Piece
}
