package com.example.student_finance_management.entities;

import com.example.student_finance_management.entities.security.User;
import com.example.student_finance_management.service.models.AddTransactionModel;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "transaction")
public class Transaction {

    @Id
    @Column(name = "transaction_Id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long transactionId;

    private double cost;

    private String name;

    @Column(name = "type")
    @Enumerated(EnumType.ORDINAL)
    private TransactionType type;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "date_id")
    private Dates date;

    @OneToMany
    @JoinColumn(name = "transaction_id")
    private List<Element> elements;

    @ManyToOne
    @JoinColumn(name = "wallet_id")
    private Wallet wallet;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public Transaction() {
    }

    public Transaction(AddTransactionModel addTransactionModel, Wallet wallet, User user, Dates date) {
        this.cost = addTransactionModel.getCost();
        this.name =  addTransactionModel.getName();
        this.type =  addTransactionModel.getType();
        this.date =  date;
        this.wallet = wallet;
        this.user = user;
    }

    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Element> getElements() {
        return elements;
    }

    public void setElements(List<Element> elements) {
        this.elements = elements;
    }

    public Dates getDate() {

        return date;
    }

    public void setDate(Dates date) {
        this.date = date;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public TransactionType getType() {
        return type;
    }

    public void setType(TransactionType type) {
        this.type = type;
    }

    public Wallet getWallet() {
        return wallet;
    }

    public void setWallet(Wallet wallet) {
        this.wallet = wallet;
    }
}
