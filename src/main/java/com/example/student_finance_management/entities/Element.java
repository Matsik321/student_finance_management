package com.example.student_finance_management.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "element")
public class Element {

    @Id
    @Column(name = "element_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long elementId;

    private double quantity;

    private double cost;

    @Column(name = "type")
    @Enumerated(EnumType.ORDINAL)
    private Type type;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "transaction_id")
    private Transaction transaction;

    private Element() {
    }

    public Element(double quantity, double cost, Type type, Product product, Transaction transaction) {
        this.quantity = quantity;
        this.cost = cost;
        this.type = type;
        this.product = product;
        this.transaction = transaction;
    }

    public Long getElementId() {
        return elementId;
    }

    public void setElementId(Long elementId) {
        this.elementId = elementId;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
